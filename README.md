# Aligent prettier config

## Usage

Create a prettier.config.js file in your project root with the following contents: 

```js
module.exports = {
  ...require("@aligent/prettier-config"),
  // override rules here. 
};
```
